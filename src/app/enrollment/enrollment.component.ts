import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Enrollment} from "../enrollment";
import { GenerateTokenService } from '../generate-token.service';
import {GenerateTokenComponent} from "../generate-token/generate-token.component";
import {Item} from "../generate-token/item";

@Component({
  selector: 'app-enrollment',
  templateUrl: './enrollment.component.html',
  styleUrls: ['./enrollment.component.css']
})
export class EnrollmentComponent implements OnInit {
  enrollment:Enrollment=new Enrollment();
  generateToken:GenerateTokenComponent=new GenerateTokenComponent();
  name = '';
  //token:any;
  nbre:number=this.generateToken.r;
  constructor() { }
  form = new FormGroup({
    lang: new FormControl('', Validators.required)
  });

  get f(){
    return this.form.controls;
  }
  async paste() {
    this.name = await navigator.clipboard.readText();

  }
  ngOnInit(): void {
  }

}
