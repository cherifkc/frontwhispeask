import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {GenerateTokenComponent} from './generate-token/generate-token.component';
import {ButtonModule} from "primeng/button";
import {FieldsetModule} from "primeng/fieldset";
import {PanelModule} from "primeng/panel";
import {ToastModule} from "primeng/toast";
import {StepsModule} from "primeng/steps";
import {CheckboxModule} from "primeng/checkbox";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {EnrollmentComponent} from './enrollment/enrollment.component';
import {ClipboardModule} from "ngx-clipboard";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";



@NgModule({
  declarations: [
    AppComponent,
    GenerateTokenComponent,
    EnrollmentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    FieldsetModule,
    BrowserAnimationsModule,
    PanelModule,
    ToastModule,
    StepsModule,
    CheckboxModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ClipboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
