import {Component, OnInit} from '@angular/core';
import {GenerateToken} from "../generate-token";
import {GenerateTokenService} from "../generate-token.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-generate-token',
  templateUrl: './generate-token.component.html',
  styleUrls: ['./generate-token.component.css']
})

export class GenerateTokenComponent implements OnInit {
  selectedFiles: File;
  message = '';
  submitted = false;
  submitted2 = false;
  user_id: string = "";
  afficher: boolean = false;
  id_user: string = "";
  enroll_msg_err: string = "";
  auth_msg_err: string = "";
  r: number = 0;
  p: number = 0;
  s: number = 0;
  s1: number = 0;
  l: number = 0;
  l1: number = 0;
  l2: number = 0;
  s2: number = 0;
  token: string;
  date_creation: string;
  date_expiration: string;
  generatetoken: GenerateToken = new GenerateToken();
  form = new FormGroup({
    lang: new FormControl('', Validators.required),
    lang1: new FormControl('', Validators.required)
  });
  name = '';
  name1 = '';
  form1 = new FormGroup({
    langu: new FormControl('', Validators.required),
    langu1: new FormControl('', Validators.required),
    fichier: new FormControl('', Validators.required)
  });

  form2 = new FormGroup({
    id: new FormControl('', Validators.required),
    langus: new FormControl('', Validators.required),
    langu2: new FormControl('', Validators.required),
    fichiers: new FormControl('', Validators.required)
  });

  constructor(private genTokenService?: GenerateTokenService, private router?: Router) {
  }

  get f() {
    return this.form.controls;
  }

  get f2() {
    return this.form1.controls;
  }

  get f3() {
    return this.form2.controls;
  }

  genToken() {
    let langue = this.form.value + "";
    console.log("langue: " + langue + "::::" + typeof (langue.toString()));
    if (langue.includes('f')) {
      langue = "fr";
    }
    if (langue.includes('e')) {
      langue = "en";
    }
    console.log("lnnnnnn: " + langue);
    this.genTokenService.generateToken(langue + "").subscribe(data => {
        this.afficher = true;
        //this.copyToken(data['token']);
        this.r++;
        console.log(data['token']);
        this.token = data['token'];
        this.date_creation = data['created_at'];
        this.date_expiration = data['expire_at'];
        //this.enrollment.copyToken();
      },
      error => console.log(error)
    )
  }

  //token:any;

  /*copyToken(tk):string{
    this.token=tk;
    return this.token;
  }*/
  ngOnInit(): void {

  }

  increment() {
    this.p++;
  }

  async paste() {
    this.name = await navigator.clipboard.readText();

  }

  async paste2() {
    this.name1 = await navigator.clipboard.readText();

  }


  //AUTH

  uploadAudio(): void {
    this.message = '';
    this.submitted = true;

    if (this.form1.invalid) {
      return;
    }
    this.upload(this.selectedFiles);
    console.log(this.selectedFiles.name);

  }

  upload(file): void {
    this.s++;
    let langue2 = this.form1.value + "";
    console.log(typeof (langue2.toString()));
    if (langue2.includes('f')) {
      langue2 = "fr";
    }
    if (langue2.includes('e')) {
      langue2 = "en";
    }
    console.log("langue: " + langue2 + " Token: " + this.token)
    this.genTokenService.enroll(file, langue2 + "", this.token).subscribe(
      data => {
        this.generatetoken.audio = this.selectedFiles.name;
        this.id_user = data['id'];
        console.log(this.id_user)
        console.log("Audio: " + this.generatetoken.audio)
        this.s1++;

        //console.log(data);
      },
      error => {
        this.s1 = 0;
        console.log(error.status)
        if (error.status == "415") {
          this.s2++;
          this.enroll_msg_err = "Fichier audio non supporté"
          console.log("Fichier audio non supporté")
        }
        if (error.status == "420") {
          this.s2++;
          this.enroll_msg_err = "Le Fichier audio ne satisfait pas les conditions"
          console.log("Le Fichier audio ne satisfait pas les conditions")
        }
      }
    );
  }

  selectFiles(e): void {
    this.selectedFiles = e.target.files[0];
  }

  upload_auth(file): void {
    this.l++;
    let langue3 = this.form1.value + "";
    console.log(typeof (langue3.toString()));
    if (langue3.includes('f')) {
      langue3 = "fr";
    }
    if (langue3.includes('e')) {
      langue3 = "en";
    }
    console.log("langue: " + langue3 + " Token: " + this.token)
    this.genTokenService.auth(file, this.user_id, langue3 + "", this.token).subscribe(
      data => {
        this.generatetoken.audio = this.selectedFiles.name;
        this.auth_msg_err = "Utilisateur authentifié avec succès"
        this.l1++;
        console.log(data);

        //console.log(data);
      },
      err => {
        this.l1 = 0;
        console.log(err.status)
        if (err.status == "415") {
          this.l2++;
          this.auth_msg_err = "Fichier audio non supporté"
          console.log("Fichier audio non supporté")
        }
        if (err.status == "420") {
          this.l2++;
          this.auth_msg_err = "Le Fichier audio ne satisfait pas les conditions"
          console.log("Le Fichier audio ne satisfait pas les conditions")
        }
        if (err.status == "419") {
          this.l2++;
          this.auth_msg_err = "Echec de l'authentification"
          console.log("Echec de l'authentification")
        }
        if (err.status == "400") {
          this.l2++;
          this.auth_msg_err = "Vérifiez vos paramètres!"

        }
      }
    );
  }

  uploadAudio_auth(): void {
    this.message = '';
    this.submitted2 = true;

    if (this.form2.invalid) {
      return;
    }
    this.upload_auth(this.selectedFiles);
    console.log(this.selectedFiles.name);
    console.log("IDDDD :" + this.user_id);

  }
}
