import {Item} from './item';

export const ITEMS: Item[] = [
  {
    name: 'Item 1',
    value: 'fr'
  },
  {
    name: 'Item 2',
    value: 'en'
  }
];
