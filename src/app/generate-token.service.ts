import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../environments/environment.prod";


@Injectable({
  providedIn: 'root'
})
export class GenerateTokenService {

  constructor(private httpclient?: HttpClient) {
  }
//GENERATION TOKEN
  generateToken(langue): Observable<Object> {
    let headers = new HttpHeaders()
    headers = headers.append('Authorization', `Bearer ${environment.api_key}`)
    headers = headers.append('Accept-Language', langue + "");
    const h = headers.get('Accept-Language')
    //console.log("Headerssss " + h);
    return this.httpclient.get(`${environment.base_url}/enroll`, {headers: headers})
  }

  //ENROLLEMENT
  enroll(file: File, langue:any, token:any): Observable<Object>{
    const formData: FormData = new FormData();
    let headers = new HttpHeaders()
    formData.append('file',file);

    headers = headers.append('Authorization', 'Bearer '+token)
    headers = headers.append('Accept-Language', langue + "");
    return this.httpclient.post(`${environment.base_url}/enroll`,formData, {headers: headers})

  }

  //AUTHENTIFICATION
  auth(file: File,id:string,langue:any, token:any): Observable<Object>{
    const formData1: FormData = new FormData();
    let headers = new HttpHeaders()
    formData1.append('file',file);
    formData1.append('id',id);

    headers = headers.append('Authorization', 'Bearer '+token)
    headers = headers.append('Accept-Language', langue + "");
    return this.httpclient.post(`${environment.base_url}/auth`,formData1, {headers: headers})

  }
}
